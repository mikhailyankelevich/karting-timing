# Karting Timing
## PLEASE, FEEL FREE TO IMPROVE! JUST SEND A MERGE REQUERST :)
### HOPE THIS LITTLE PROJECT WILL HELP WITH NON-PROFESSIONAL KARTING TRAINING 

## Overview

This project uses node-red flow with dashboard installed, which runs a script
sending your account a private message from another account with dev access.
On the phone itself the message can be read into the headphones by turning
ios voice-over on.

## Instructions

1. Install node-red (sudo apt install node-red)
2. Install node-red-dashboard 
(sudo npm install node-red-dashboard --unsafe-perm=true -g now)
3. Install pm2 (sudo npm install pm2 --unsafe-perm=true -g now) 
4. Run the startup script setup.sh 
(will restart automatically by pm2, so 1 USE ONLY)
5. Access 'sender' web  page by localhost:1880/ui 


NOTE:
	* node-red will be run on port 1880
	* to shut down the system do it through sudo pm2 stop ...